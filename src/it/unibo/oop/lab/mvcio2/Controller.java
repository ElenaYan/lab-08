package it.unibo.oop.lab.mvcio2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JOptionPane;

/**
 * 
 */
public class Controller {

    /**
     * This class implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     */
    public static final String SEP = File.separator;

    /**
     * The String representing the folder where is saving the file.
     */
    private static String FILE_NAME = System.getProperty("user.home") + SEP + "output.txt";

    private File file;

    /**
     * method that initialize fields.
     * @throws IOException if an I/O error occurs.
     */
    public Controller() throws IOException {
        this.file = new File(FILE_NAME);
    }


    /**
     * method for getting the current File.
     * @return current file
     */
    public File getFile() {
        return this.file;
    }


    /**
     * method for setting the current File.
     * @param path  file path to setting a new file
     */
    public void setFile(final String path) {
        FILE_NAME = path;
        this.file = new File(FILE_NAME);
    }

    /**
     * method for getting the current File Path.
     * @return current file path
     */
    public String getPath() {
        return this.file.getPath();
    }

    /**
     * method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * @param text  text to save
     * @throws IOException if an I/O error occurs.
     */
    public static void save(final String text) throws IOException {
        try (PrintStream ps = new PrintStream(FILE_NAME)) {
            ps.print(text);
        } catch (FileNotFoundException e1) {
            JOptionPane.showMessageDialog(null, e1, "Error", JOptionPane.ERROR_MESSAGE);
        }

    }


}
