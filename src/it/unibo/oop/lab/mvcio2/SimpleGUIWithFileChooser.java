package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

    private static final int PROPORTION = 2;

    private final JFrame frame = new JFrame();

    public SimpleGUIWithFileChooser() throws IOException {
        final Controller file = new Controller();
        
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int height = (int) screen.getHeight();
        final int width = (int) screen.getWidth();
        final JPanel panelText = new JPanel();
        final JPanel panelFile = new JPanel();
        final JButton save = new JButton("Save");
        final JButton browse = new JButton("Browse...");
        final JTextArea text = new JTextArea();
        final JLabel fileName = new JLabel(file.getPath());

        frame.setSize(width / PROPORTION, height / PROPORTION);
        frame.setTitle("My second Java graphical interface");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        panelText.setLayout(new BorderLayout());
        panelFile.setLayout(new BorderLayout());

        panelFile.add(fileName, BorderLayout.CENTER);
        panelFile.add(browse, BorderLayout.LINE_END);

        panelText.add(panelFile, BorderLayout.NORTH);
        panelText.add(save, BorderLayout.SOUTH);
        panelText.add(text);

        frame.setContentPane(panelText);
        frame.getContentPane().add(panelFile, BorderLayout.NORTH);
        frame.setVisible(true);
    
        save.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Controller.save(text.getText());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            
        });
        
        browse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFileChooser choose = new JFileChooser(file.getPath());
                if(choose.showSaveDialog(fileName) == JFileChooser.APPROVE_OPTION) {
                    file.setFile(choose.getSelectedFile().getPath());
                    fileName.setText(file.getPath());
                    System.out.println(file.getPath() + choose.getSelectedFile().getPath());
                    
                }
               
                
            }
        });
    }

    public static void main(final String[] args) throws IOException {
        new SimpleGUIWithFileChooser();
    }

}
