package it.unibo.oop.lab.iogui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.Random;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;



/**
 * This class is a simple application that writes a random number on a file.
 * 
 * This application does not exploit the model-view-controller pattern, and as
 * such is just to be used to learn the basics, not as a template for your
 * applications.
 */
public class BadIOGUI {

    private static final String TITLE = "A very simple GUI application";
    private static final String PATH = System.getProperty("user.home")
            + System.getProperty("file.separator")
            + BadIOGUI.class.getSimpleName() + ".txt";
    private final Random rng = new Random();
    private final JFrame frame = new JFrame(TITLE);

    /**
     * 
     */
    public BadIOGUI() {
        final JPanel canvas = new JPanel();
        final JPanel canvas2 = new JPanel();

        canvas2.setLayout(new BoxLayout(canvas2, BoxLayout.LINE_AXIS));
        canvas.setLayout(new BorderLayout());

        final JButton write = new JButton("Write on file");
        final JButton read = new JButton("Read from file");

        canvas2.add(write);
        canvas2.add(read);
        canvas.add(canvas2, BorderLayout.CENTER);

        frame.setContentPane(canvas);
        frame.getContentPane().add(canvas2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*
         * Handlers
         */
        write.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {

                try (PrintStream ps = new PrintStream(PATH)) {
                    ps.print(rng.nextInt());
                } catch (FileNotFoundException e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
            }
        });

        read.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                try {
                    final List<String> lines = Files.readAllLines(new File(PATH).toPath());
                    for (final String line: lines) {
                        System.out.println(line);
                    }
               } catch (IOException e1) {
                   JOptionPane.showMessageDialog(frame, e1, "Error" , JOptionPane.ERROR_MESSAGE);
                   e1.printStackTrace();
               }

//                try (PrintStream stream = new PrintStream(PATH)) {
//                    stream.print();
//                } catch (FileNotFoundException e1) {
//                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
//                    e1.printStackTrace();
//                }
            }

        });
    }

    private void display() {

        frame.pack(); 
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        /*
         * OK, ready to pull the frame onscreen
         */
        frame.setVisible(true);
    }

    /**
     * @param args ignored
     */
    public static void main(final String... args) {
       new BadIOGUI().display();
    }
}
